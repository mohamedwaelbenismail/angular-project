#!/bin/bash
#Get servers list
set -f
string=$DEPLOY_SERVER
array=("${string//,/ }")
#Iterate servers for deploy and pull last commit
for i in "${!array[@]}"
do    
      echo "Deploy project on server ${array[i]}"    
      ssh ec2-user@"${array[i]}" "sudo su && docker stop hama && docker rm hama && echo $CI_BUILD_TOKEN | docker login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY && docker pull  "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"  && docker run --name hama -t -i -p 3000:3000 -d  "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG" "
done
