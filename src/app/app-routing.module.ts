import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { UserProfileComponent } from './manage-user/user-profile/user-profile.component';
import { DetailCourseComponent } from './manage-courses/detail-course/detail-course.component';
import { HomeComponent } from './home/home.component';



const routes: Routes = [
{
  path: '',
  component: HeaderComponent,
   children: [
    {
      path: '',
      redirectTo: 'home',
      pathMatch: 'full'
    },
    {
      path: 'user',
      loadChildren: () => import('./manage-user/manage-user.module').then(m => m.ManageUSerModule)
      },
      {
        path: 'home',
        component: HomeComponent
      }
   ],
}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
