import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  myForm: FormGroup;
  isAuth = true;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.myForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      code: ['', [Validators.required, Validators.nullValidator]]
    });
  }

  get email() {
    return this.myForm.get('email');
  }
  get code() {
    return this.myForm.get('code');
  }

  Reset(myForm) {
    console.log(myForm);
    return this.isAuth = false;
  }

}
