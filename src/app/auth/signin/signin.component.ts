import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Utils } from 'src/app/shared/Utils/Utils';

 @Component({
    selector: 'app-signin',
   templateUrl: './signin.component.html',
   styleUrls: ['./signin.component.css']
 })
export class SigninComponent implements OnInit {

  myForm: FormGroup;
  hasAnAccount=true;

  constructor(private fb: FormBuilder) {
  }

   ngOnInit(): void {
  
     this.myForm = this.fb.group({
       email: ['', [Validators.required, Validators.email]],
       password: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9]{6,20}')]],
       agree: [false]
     });
    

    Utils.initializeSelect2();
   }
   get email() {
     return this.myForm.get('email');
   }
   get password() {
     return this.myForm.get('password');
   }
   get agree() {
     return this.myForm.get('agree');
   }

   login(myForm) {
    console.log(myForm);
   }

   changeModalToSignup(){
     this.hasAnAccount=false;
   }
}
