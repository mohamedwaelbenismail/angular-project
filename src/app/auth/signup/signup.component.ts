import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  myForm: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.myForm = this.fb.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      userName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9]{6,20}')]],
      rpassword: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9]{6,20}')]],
      age: [null, [Validators.required, Validators.minLength(2), Validators.min(18), Validators.max(65)]],
      agree: [false, [Validators.requiredTrue]]
    });
  }
  get firstName() {
    return this.myForm.get('firstName');
  }
  get lastName() {
    return this.myForm.get('lastName');
  }
  get userName() {
    return this.myForm.get('userName');
  }
  get email() {
    return this.myForm.get('email');
  }
  get password() {
    return this.myForm.get('password');
  }
  get rpassword() {
    return this.myForm.get('rpassword');
  }
  get age() {
    return this.myForm.get('age');
  }
  get agree() {
    return this.myForm.get('agree');
  }

  register(myForm) {
    console.log(myForm);
  }
}
