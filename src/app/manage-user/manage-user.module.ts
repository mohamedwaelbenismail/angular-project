
import { NgModule } from '@angular/core';

import {FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { CommonModule } from '@angular/common';
import { ManageUSerRooting } from './manage-user-routing.module';


@NgModule({
  declarations: [
    UserProfileComponent,
  ],
  imports: [
    CommonModule,
    ManageUSerRooting,
    FormsModule,
    HttpClientModule
  ],
  providers: [],

  exports: [
    UserProfileComponent
  ]
})
export class ManageUSerModule { }
