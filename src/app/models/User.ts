export class User {
  firstName: string;
  lastName: string;
  userName: string;
  datNaiss: Date;
  description: string;


  constructor(firstName: string, lastName: string, userName: string, datNaiss: Date, description: string) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.userName = userName;
    this.datNaiss = datNaiss;
    this.description = description;
  }
}
