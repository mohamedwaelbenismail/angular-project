import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  constructor(private http:HttpClient) {

   }
   createUser(User){
     const link="http://localhost:8000/api/register"
     return this.http.post(link,User)
   }
   Login(User):Observable<any>{
     const link="http://localhost:8000/api/login"
     return this.http.post<any>(link,User)
   }
}
